import fs from "fs"
import TurndownService from "turndown"
import CTZN from "./ctzn.js"
import express from "express"
import cors from 'cors'
import https from 'https'

const app = express()
app.use(cors())
let config = JSON.parse(fs.readFileSync('config.json', 'utf-8'))
let garden = config.ctzn.garden
let turndownService = new TurndownService()
let userId = config.ctzn.uri
let [userName, userHost] = userId.split("@")
let userPass = config.ctzn.pass
let ctzn = new CTZN({ name: userName, host: userHost, pass: userPass })

const writePage = async (userId, page) => {
    let pageName = page.value.title
    if(pageName.match(/agora-prefix-/)){
        pageName = pageName.replace("agora-prefix-","")
    }
    const blobName = page.value.content.blobName
    const blob = await ctzn.apiCall("blob.get", [userId, blobName])
    let content = turndownService.turndown(atob(blob.buf))
    const dirpath = `${garden}/${userId}`

    content = content.replace(/\\\[/g, "[").replace(/\\\]/g, "]")
    fs.mkdirSync(dirpath, { recursive: true })
    let file = `${garden}/${userId}/${pageName}.md`
    console.log(`writing ${file}`)
    fs.writeFileSync(file, content)
}

const getPage = async (userId, pageName) => {
    if(pageName.match(/\d/)){
        pageName = `agora-prefix-${pageName}`
    }
    console.log("userId", userId)
    return await ctzn.apiCall("table.get", [userId, "ctzn.network/page", pageName])
}


const start = async () => {
    await ctzn.connect()
    writePages()
    setInterval(writePages, 30e3)
}

const writePages = async () => {
    const following = await ctzn.getFollowing(userId)
    for (const userId of following) {
        const pages = await ctzn.getPages(userId)
        for (const page of pages) {
            writePage(userId, page)
        }
    }
}

try {
    await start()
} catch (e) {
    console.error("failture in start loop", e)
    start()
}

app.get('/:userId-:pageName', async (req, res) => {
    try {
        console.log("params", req.params)
        let page = await getPage(req.params.userId, req.params.pageName)
        console.log("page", page)
        await writePage(req.params.userId, page)
        res.send('Page updated')
    } catch (e) {
        console.error("failure in get loop", e)
    }
})



if (config.ctzn.http) {
    app.listen(config.ctzn.port)
} else {
    let key = fs.readFileSync(config.ctzn.key)
    let cert = fs.readFileSync(config.ctzn.cert)
    https.createServer({ key, cert }, app).listen(config.ctzn.port)
}


